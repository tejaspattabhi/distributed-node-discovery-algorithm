remoteuser=txp130630
remotecomputer1=dc44
remotecomputer2=dc44
remotecomputer3=dc39
remotecomputer4=dc42
remotecomputer5=dc37

# ConfigFile Custom name
configFile=config

# Compiling
g++ nodeDiscovery.cpp -o nodeDiscovery -l sctp -std=c++0x -pthread

ssh -l "$remoteuser" "$remotecomputer1" "cd $HOME/AOS;./nodeDiscovery 1 $configFile" &
ssh -l "$remoteuser" "$remotecomputer2" "cd $HOME/AOS;./nodeDiscovery 2 $configFile" &
ssh -l "$remoteuser" "$remotecomputer3" "cd $HOME/AOS;./nodeDiscovery 3 $configFile" &
ssh -l "$remoteuser" "$remotecomputer4" "cd $HOME/AOS;./nodeDiscovery 4 $configFile" &
ssh -l "$remoteuser" "$remotecomputer5" "cd $HOME/AOS;./nodeDiscovery 5 $configFile" &

