#include <iostream>
using namespace std;
#include <fstream>
#include <queue>

#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/sctp.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <time.h>
#include <pthread.h>

#define SIZE 		100
#define MAX_NODES 	45
#define MAX_BUFFER	1024

char configFile [SIZE];

class Neighbor {
	private:
		int 		neighID;
		char		hostName [SIZE];
		int			pNum;
	public:
		Neighbor ();
		Neighbor (int);
		Neighbor (char *);
		Neighbor (int, char *, int);
		
		void setNeighID (int);
		int getNeighID ();
		
		void setHostName (char *);
		void getHostName (char *);
		
		void setPortNumber (int);
		int getPortNumber ();
		
		bool isPresent (Neighbor *, int);
};

Neighbor::Neighbor () {
	neighID 	= -1;
	pNum 		= -1;
	strcpy (hostName, "");
}

Neighbor::Neighbor (int neighID) {
	ifstream 	ifile;
	char 		buffer [SIZE];
	char 		nodeID [SIZE];
	char 		hostName [SIZE];
	char 		pNum [SIZE];
	
	// Will be discarded. Using to move through the file.
	int neighs [SIZE];
	int nCount;
	
	ifile.open (configFile);
	ifile >> buffer;
	strcpy (buffer, "");
	
	this->neighID = neighID;
	
	while (!ifile.eof ()) {
		nCount = -1;
	
		ifile >> nodeID;
		ifile >> hostName;
		ifile >> pNum;
		
		while (strcmp (buffer, "#")) {
			if (ifile.eof ())
				break;
				
			ifile >> buffer;
			neighs [++nCount] = atoi (buffer);
		}
		strcpy (buffer, "");
	
		if (atoi (nodeID) == neighID) {
			this->pNum = atoi (pNum);
			strcpy (this->hostName, hostName);
			ifile.close ();
			
			return;
		}
	}
	ifile.close ();
}

Neighbor::Neighbor (char * buffer) {
	char 	hostName [SIZE];
	int		pNum;
	int 	id;
	
	char * str = NULL;
	str = strtok (buffer, ",");
	id = atoi (str);
	
	str = strtok (NULL, ",");
	strcpy (hostName, str);
	
	str = strtok (NULL, ",");
	pNum = atoi (str);
	
	// cout << id << " " << hostName << " " << pNum << endl;
	
	this->neighID 	= id;
	this->pNum 		= pNum;
	strcpy (this->hostName, hostName);
}

Neighbor::Neighbor (int neighID, char * hostName, int pNum) {
	this->neighID 	= neighID;
	this->pNum 		= pNum;
	strcpy (this->hostName, hostName);
}

void Neighbor::setNeighID (int temp) {
	this->neighID = temp;
}
int Neighbor::getNeighID () {
	return this->neighID;
}

void Neighbor::setHostName (char * buffer) {
	strcpy (this->hostName, buffer);
}

void Neighbor::getHostName (char * buffer) {
	strcpy (buffer, this->hostName);
}

void Neighbor::setPortNumber (int temp) {
	this->pNum = temp;
}

int Neighbor::getPortNumber () {
	return this->pNum;
}

bool Neighbor::isPresent (Neighbor * visitedNodes, int count) {
	for (int i = 0; i < count; i++) {
		if (visitedNodes[i].getNeighID () == this->neighID)
			return true;
	}
	return false;
}

class Node {
	private:
		int 			nodeID;
		char 			hostName [SIZE];
		int 			pNum;
		Neighbor 		neighbors [MAX_NODES];
		int 			neighCount;
	public:
		Node ();
		Node (int);
		Node (int, char *, int, Neighbor *, int);
		
		void setNodeID (int);
		int getNodeID ();
		
		void setHostName (char *);
		void getHostName (char *);
		
		void setPortNumber (int);
		int getPortNumber ();
		
		void setNeighbors (Neighbor *);
		void setNeighbors (Neighbor *, int);
		Neighbor getNeighbor (int);
		int getNeighborsID (int);
		
		void setNeighCount (int);
		int getNeighCount ();
		
		void packNeighbors (char *);
};

Node::Node (int nodeID) {
	this->nodeID 		= nodeID;
	this->pNum 			= -1;
	this->neighCount 	= 0;
	strcpy (this->hostName, "");
}

Node::Node () {
	this->nodeID 		= -1;
	this->pNum 			= -1;
	this->neighCount 	= 0;
	strcpy (this->hostName, "");
}

Node::Node (int nodeID, char * hostName, int pNum, Neighbor * neighs, int nCount) {
	this->nodeID = nodeID;
	strcpy (this->hostName, hostName);
	this->pNum = pNum;
	this->neighCount = nCount;
	
	for (int i = 0; i < nCount; i++) {
		this->neighbors[i] = neighs[i];
	}
}

void Node::setNodeID (int temp) {
	this->nodeID = temp;
}

int Node::getNodeID () {
	return this->nodeID;
}

void Node::setHostName (char * buffer) {
	strcpy (this->hostName, buffer);
}

void Node::getHostName (char * buffer) {
	strcpy (buffer, this->hostName);
}

void Node::setPortNumber (int temp) {
	this->pNum = temp;
}

int Node::getPortNumber () {
	return this->pNum;
}

void Node::setNeighbors (Neighbor * temp) {
	for (int i = 0; i < this->neighCount; i++) 
		this->neighbors[i] = temp[i];
}

void Node::setNeighbors (Neighbor * temp, int tCount) {
	this->neighCount = tCount;
	for (int i = 0; i < this->neighCount; i++) 
		this->neighbors[i] = temp[i];
}

int Node::getNeighborsID (int i) {
	if (i < this->neighCount)
		return this->neighbors[i].getNeighID ();
	else
		return -1;
}

Neighbor Node::getNeighbor (int i) {
	if (i < this->neighCount)
		return this->neighbors[i];
	else
		return Neighbor ();
}

void Node::setNeighCount (int temp) {
	this->neighCount = temp;
}

int Node::getNeighCount () {
	return this->neighCount;
}

void Node::packNeighbors (char * buffer) {
	char temp [MAX_BUFFER];
	char hostname [SIZE];
	
	strcpy (buffer, "");
	for (int i = 0; i < neighCount; i++) {
		neighbors[i].getHostName (hostname);
		sprintf (temp, "%d,%s,%d#", neighbors[i].getNeighID (), hostname, neighbors[i].getPortNumber ());
		strcat (buffer, temp);
	}
}

Node initializeCurrentNode (int);
void serverThread (Node);

Node initializeCurrentNode (int thisNodeID) {
	ifstream 		ifile;
	char 			buffer [SIZE];
	char 			nodeID [SIZE];
	char 			hostName [SIZE];
	char 			pNum [SIZE];
	int 			neighs [MAX_NODES];
	Neighbor 		neighObj [MAX_NODES];
	int 			nCount;
	
	ifile.open (configFile);
	ifile >> buffer;
	strcpy (buffer, "");
	
	while (!ifile.eof ()) {
		nCount = -1;
	
		ifile >> nodeID;
		ifile >> hostName;
		ifile >> pNum;
		
		while (strcmp (buffer, "#")) {
			if (ifile.eof ())
				break;
				
			ifile >> buffer;
			neighs [++nCount] = atoi (buffer);
		}
		strcpy (buffer, "");
	
		if (atoi (nodeID) == thisNodeID) {
			ifile.close ();
			
			for (int i = 0; i < nCount; i++)
				neighObj[i] = Neighbor (neighs[i]);
			
			return Node (thisNodeID, hostName, atoi (pNum), neighObj, nCount);
		}
	}
	ifile.close ();
	return Node ();
}

void * serverThread (void * tArg) {
	char		neighbors [MAX_BUFFER];
	char 		buffer [MAX_BUFFER + 1];
	int 		listenSock, connSock, ret, in, flags;
	struct 		sockaddr_in 			servaddr;
	struct 		sctp_initmsg 			initmsg;
	struct 		sctp_event_subscribe 	events;
	Node		node;
	Node 		* arg;
	
	arg = (Node *) tArg;
	node = * arg;
	
	memset (neighbors, 0, MAX_BUFFER);
	node.packNeighbors (neighbors);
	
	cout << "Node " << node.getNodeID () << " -- " << "Thread for public listening initiated." << endl;
	listenSock = socket (AF_INET, SOCK_STREAM, IPPROTO_SCTP);

	bzero ((void *) &servaddr, sizeof (servaddr));
	servaddr.sin_family 		= AF_INET;
	servaddr.sin_addr.s_addr 	= htonl (INADDR_ANY);
	servaddr.sin_port 			= htons (node.getPortNumber ());

	ret = ::bind (listenSock, (struct sockaddr *) &servaddr, (long) sizeof (servaddr));
	
	/* Specify that a maximum of 5 streams will be available per socket */
	memset (&initmsg, 0, sizeof (initmsg));
	initmsg.sinit_num_ostreams 		= 5;
	initmsg.sinit_max_instreams 	= 5;
	initmsg.sinit_max_attempts 		= 4;
	ret = setsockopt (listenSock, IPPROTO_SCTP, SCTP_INITMSG, &initmsg, sizeof (initmsg));
	listen (listenSock, 5);
	
	while (1) {
		/*Awaiting a new connection*/
		memset (buffer, 0, MAX_BUFFER + 1);
		connSock = accept (listenSock, (struct sockaddr *) NULL, (socklen_t *) NULL);
		
		if (connSock == -1) {
			perror ("accept ()");
			exit (-1);
		}
		
		in = sctp_recvmsg (connSock, (void *) buffer, sizeof (buffer), (struct sockaddr *) NULL, 0, 0, 0);
		cout << "Node " << node.getNodeID () << " -- " << "Message Received: " << buffer << endl;
		
		if (!strcmp (buffer, "REQUEST_NEIGHBORS")) {
			ret = sctp_sendmsg (connSock, (void *) neighbors, (size_t) strlen (neighbors), NULL, 0, 0, 0, 0, 0, 0);
			cout << "Node " << node.getNodeID () << " -- " << "Information regarding Neighbors sent " << endl;
		}
		
		close (connSock);
	}
}

int main (int argc, char * argv[]) {
	Node 				node;
	int 				thisNodeID;
	queue<Neighbor>		vNodes;
	Neighbor 			visitedNodes [MAX_NODES];
	int					vNodeCount;
	ofstream 			ofile;
	
	// Thread Variables
	int			thID;
	pthread_t	server;
	
	// A SCTP Client Variables
	int 		connSock, in, ret, flags;
	struct 		hostent * 		pHostInfo;
	struct 		sockaddr_in 	servaddr;
	struct 		sctp_status 	status;
	long 		nHostAddress;
	struct 		sctp_sndrcvinfo sndrcvinfo;
	
	char 		buffer [MAX_BUFFER + 1];
	char 		strHostName [MAX_BUFFER];
	char		neighbors [MAX_BUFFER];
	char		pNeighs [10][SIZE];
	int 		pNeighCount;
	
	vNodeCount = 0;
	pNeighCount = 0;
	strcpy (configFile, "config");
	
	if (argc < 2) {
		cout << "Usage: " << argv[0] << " <Unique Node ID> [<Custom \"Config File Name\"> or default taken as \"config\"]" << endl;
		return -1;
	}
	
	if (argc == 3) {
		strcpy (configFile, argv[2]);
	}
	
	thisNodeID = atoi (argv[1]);
	node = initializeCurrentNode (thisNodeID);
	memset (neighbors, 0, MAX_BUFFER);
	node.packNeighbors (neighbors);

	sprintf (buffer, "%d", node.getNodeID());	
	ofile.open (buffer);
	
	cout << "Information on Node " << node.getNodeID () << ":" << endl;
	node.getHostName (buffer);
	cout << "Host: " << buffer << endl;
	cout << "Port Number: " << node.getPortNumber () << endl;
	cout << "Number of Neighbors: " << node.getNeighCount () << endl;
	
	ofile << "Information on Node " << node.getNodeID () << ":" << endl;
	ofile << "Host: " << buffer << endl;
	ofile << "Port Number: " << node.getPortNumber () << endl;
	ofile << "Number of Neighbors: " << node.getNeighCount () << endl;
	
	for (int i = 0; i < node.getNeighCount (); i++) {
		cout << node.getNeighborsID (i) << "\t";
		ofile << node.getNeighborsID (i) << "\t";
	}
	cout << endl << endl;
	ofile << endl << endl;
	
	thID = pthread_create (&server, NULL, serverThread, (void *) &node);
	if (thID) {
		cout << "\nError Creating a thread\nDying...\n" << endl;
		ofile << "\nError Creating a thread\nDying...\n" << endl;
		
		ofile.close ();
		return 1;
	}
	
	sleep (2);
	// Implement main logic
	
	// Visiting myself
	node.getHostName (buffer);
	visitedNodes [vNodeCount++] = Neighbor (node.getNodeID (), buffer, node.getPortNumber ());
	
	for (int i = 0; i < node.getNeighCount (); i++) {
		vNodes.push (node.getNeighbor (i));
		visitedNodes [vNodeCount++] = node.getNeighbor (i);
	}
	
	cout << "Node " << node.getNodeID () << " -- " << "Initially Known Nodes: ";
	cout << visitedNodes[0].getNeighID ();
	for (int i = 1; i < vNodeCount; i++)
		cout << ", " << visitedNodes[i].getNeighID ();
	cout << endl;
	
	ofile << "Initially Known Nodes: ";
	ofile << visitedNodes[0].getNeighID ();
	for (int i = 1; i < vNodeCount; i++)
		ofile << ", " << visitedNodes[i].getNeighID ();
	ofile << endl;
	
	while (!vNodes.empty ()) {
		cout << "Node " << node.getNodeID () << " -- " << "Contacting Node " << vNodes.front ().getNeighID () << " for its neighbors." << endl;
		ofile << "Contacting Node " << vNodes.front ().getNeighID () << " for its neighbors." << endl;
		
		// Remove Considered Node from the Queue.
		Neighbor nNode = vNodes.front ();
		vNodes.pop ();
		
		connSock = socket (AF_INET, SOCK_STREAM, IPPROTO_SCTP);
		if (connSock == -1) {
			perror ("socket ()");
			
			ofile.close ();
			return -1;
		}

		bzero ((void *) &servaddr, sizeof (servaddr));
		
		nNode.getHostName (strHostName);
		cout << "Node " << node.getNodeID () << " -- " << "Node " << nNode.getNeighID () << ": " << strHostName << ", " << nNode.getPortNumber () << endl;
		ofile << "Node " << nNode.getNeighID () << ": " << strHostName << ", " << nNode.getPortNumber () << endl;
		
		pHostInfo = gethostbyname (strHostName);
		memcpy (&nHostAddress, pHostInfo->h_addr, pHostInfo->h_length);
		
		servaddr.sin_family 		= AF_INET;
		servaddr.sin_port 			= htons (nNode.getPortNumber ());
		servaddr.sin_addr.s_addr 	= nHostAddress;
		
		if (connect (connSock, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0){
			perror ("connect ()");
			return -1;
		}

		memset (buffer, 0, MAX_BUFFER + 1);
		strcpy (buffer, "REQUEST_NEIGHBORS");
		
		ret = sctp_sendmsg (connSock, (void *) buffer, (size_t) strlen (buffer), NULL, 0, 0, 0, 0, 0, 0 );
		cout << "Node " << node.getNodeID () << " -- " << "Contacted Node " << nNode.getNeighID () << " for its neighbors. Waiting for results." << endl;
		ofile << "Contacted Node " << nNode.getNeighID () << " for its neighbors. Waiting for results." << endl;
		
		memset (buffer, 0, MAX_BUFFER + 1);
		
		in = sctp_recvmsg (connSock, (void *) buffer, sizeof (buffer), (struct sockaddr *) NULL, 0, &sndrcvinfo, &flags);
		
		while (!strcmp (buffer, "REQUEST_NEIGHBORS")) {
			sctp_sendmsg (connSock, (void *) neighbors, (size_t) strlen (neighbors), NULL, 0, 0, 0, 0, 0, 0 );
			memset (buffer, 0, MAX_BUFFER + 1);
			// strcpy (buffer, "REQUEST_NEIGHBORS");
			// ret = sctp_sendmsg (connSock, (void *) buffer, (size_t) strlen (buffer), NULL, 0, 0, 0, 0, 0, 0 );
			sctp_recvmsg (connSock, (void *) buffer, sizeof (buffer), (struct sockaddr *) NULL, 0, &sndrcvinfo, &flags);
		}
		cout << "Node " << node.getNodeID () << " -- " << "Neighbor Information Received" << endl;
		ofile << "Neighbor Information Received" << endl;
		
		char * sPtr = NULL;
		// cout << "Node " << node.getNodeID () << " -- " << buffer << endl;
		// ofile << "Node " << node.getNodeID () << " -- " << buffer << endl;
		
		
		pNeighCount = 0;
		sPtr = strtok (buffer, "#");
		while (sPtr != NULL) {
			strcpy (pNeighs [pNeighCount++], sPtr);
			sPtr = strtok (NULL, "#");
		}
		
		// for (int i = 0; i < pNeighCount; i++)
			// cout << "Node " << node.getNodeID () << " -- " << pNeighs [i] << endl;
		
		for (int i = 0; i < pNeighCount; i++) {
			// cout << "Node " << node.getNodeID () << " -- " << sPtr << endl;
			// ofile << "Node " << node.getNodeID () << " -- " << sPtr << endl;
			// cout << "Processing: " << pNeighs[i] << endl;
			// ofile << "Processing: " << pNeighs[i] << endl;
			
			Neighbor temp = Neighbor (pNeighs[i]);
			cout << "Node " << node.getNodeID () << " -- " << "Discovered Neighbor Information: " << temp.getNeighID () << ", inserting in the queue if not visited." << endl;
			ofile << "Discovered Neighbor Information: " << temp.getNeighID () << ", inserting in the queue if not visited." << endl;
			
			if (!temp.isPresent (visitedNodes, vNodeCount)) {
				cout << "Node " << node.getNodeID () << " -- Not visited Node " << temp.getNeighID () << ". So inserting into the queue." << endl;
				ofile << "Not visited Node " << temp.getNeighID () << ". So inserting into the queue. " << endl;
				
				vNodes.push (temp);
				visitedNodes [vNodeCount++] = temp;
			}
			
			// sPtr = strtok (NULL, "#");
			// cout << "Node " << node.getNodeID () << " -- " << sPtr << endl;
			// ofile << "Node " << node.getNodeID () << " -- " << sPtr << endl;
		}
		close (connSock);
		
		cout << "Node " << node.getNodeID () << " -- " << "Nodes Discovered Till Now: ";
		cout << visitedNodes[0].getNeighID ();
		for (int i = 1; i < vNodeCount; i++)
			cout << ", " << visitedNodes[i].getNeighID ();
		cout << endl;
		
		ofile << "Nodes Discovered Till Now: ";
		ofile << visitedNodes[0].getNeighID ();
		for (int i = 1; i < vNodeCount; i++)
			ofile << ", " << visitedNodes[i].getNeighID ();
		ofile << endl;
		
		// sleep (1);
	}
	
	cout << "Node " << node.getNodeID () << " -- " << "Node Discovery Algorithm Completed. Terminating..." << endl;
	cout << "Node " << node.getNodeID () << " -- " << "Nodes of the Graph: ";
	cout << visitedNodes[0].getNeighID ();
	for (int i = 1; i < vNodeCount; i++)
		cout << ", " << visitedNodes[i].getNeighID ();
	cout << endl;
	
	ofile << "Node Discovery Algorithm Completed. Terminating..." << endl;
	ofile << "Nodes of the Graph: ";
	ofile << visitedNodes[0].getNeighID ();
	for (int i = 1; i < vNodeCount; i++)
		ofile << ", " << visitedNodes[i].getNeighID ();
	ofile << endl;
	ofile.close ();
	
	// Wait to give your information to all other nodes.
	sleep (5);
	return 0;
}
