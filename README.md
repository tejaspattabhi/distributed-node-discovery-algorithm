AOS Project 1 -- Distributed Node Discovery Algorithm
==
Project Coded in C++
--

#### Please follow the steps for execution:


**Step 1**: To compile, log on to `CS1/CS2.utdallas.edu` machines ONLY. This is 
		because the DC Machines DO NOT HAVE SCTP Libraries to be linked for C/C++.

**Step 2**: Create your own knowledge graph in the format:

    # <NODE ID1>	<HOSTNAME>	<PORT>	<List of Neighbors separated by spaces>
    # <NODE ID2>	<HOSTNAME> 	<PORT>	<List of Neighbors separated by spaces>
    # ..
    
Save the filename as "config" or some custom name.

**Step 3**: If a custom file (name which is not "config") is created, edit 
		"initialize.sh", and change the "configFile" to your custom name.

**Step 4**: Edit the "**initialize.sh**" for few more parameters:

- User Executing the code should replace his/her NetID against the value of "remoteuser" for successful 
   execution. (Unless it is the Super User.)

- MAKE SURE THAT THE (NODE ID, HOSTNAME)s LISTED ON THE KNOWLEDGE GRAPH FILE
   ARE LISTED AS SAME (VALUE AND ORDER) IN THE "remotecomputer" variables.
		   
**Step 5**: Execution:

Run the command:

    cd *location_for_all_the_files_listed_in_Note_2_below*
    sh initialize.sh
    	   
**Step 6**: Output -- The Output can be seen on screen (mixed with all node processes)
		or can also be viewed in a file for each node.
		Output file Names is after the <NODE ID>
		Example, if the node ID is "1", the output file created is also "1"
		By, "cat 1", you can view the output logs.
		
#################################################################################
Note:

1. If you want to compile the code manually (note with the initialize.sh script)
   then,

    	g++ nodeDiscovery.cpp -o nodeDiscovery -l sctp -std=c++0x -pthread
   
2. Make sure that the following files are in the same folder for successful
   execution
   - `initialize.sh`
   - `nodeDiscovery.cpp`
   - `config` "or" `custom_knowledge_graph_file`
#################################################################################

*Please contact me on any kind of concerns/questions*

**By,** 
### Tejas Tovinkere Pattabhi 
**Email: tejas.pattabhi@gmail.com/txp130630@utdallas.edu**

**Net ID: txp130630**